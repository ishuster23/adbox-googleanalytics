# Adbox-googleAnalytics

[![CI Status](https://img.shields.io/travis/Aleks C. Barragan/Adbox-googleAnalytics.svg?style=flat)](https://travis-ci.org/Aleks C. Barragan/Adbox-googleAnalytics)
[![Version](https://img.shields.io/cocoapods/v/Adbox-googleAnalytics.svg?style=flat)](https://cocoapods.org/pods/Adbox-googleAnalytics)
[![License](https://img.shields.io/cocoapods/l/Adbox-googleAnalytics.svg?style=flat)](https://cocoapods.org/pods/Adbox-googleAnalytics)
[![Platform](https://img.shields.io/cocoapods/p/Adbox-googleAnalytics.svg?style=flat)](https://cocoapods.org/pods/Adbox-googleAnalytics)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Adbox-googleAnalytics is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Adbox-googleAnalytics'
```

## Author

Aleks C. Barragan, leks.bar@icloud.com

## License

Adbox-googleAnalytics is available under the MIT license. See the LICENSE file for more info.
